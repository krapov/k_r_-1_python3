def matrix_multiplication(m1, m2):
    '''умножает две матрицы'''
    m3 = [[0 for j in range(len(m1))] for i in range(len(m2))]
    for i in range(len(m1)):
        for j in range(len(m2)):
            for k in range(len(m1)):
                m3[i][j] += m1[i][k] * m2[k][j]
    return m3