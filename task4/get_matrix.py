import random

def generate_matrix_int(n):
    '''Генерирует целочисленную матрицу заданной размерности'''
    matrix = [[random.randint(0, 10) for j in range(n)] for i in range(n)]
    return matrix

def generate_matrix_float(n):
    '''Генерирует вещественную матрицу заданной размерности'''
    matrix = [[random.uniform(0, 10) for j in range(n)] for i in range(n)]
    return matrix
