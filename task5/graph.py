import matplotlib.pyplot as plt


def graph(x, y):
    '''Строит график по заданным точкам'''
    plt.title('график зависимости времени решения системы от её размерности')
    plt.xlabel('Размерность')
    plt.ylabel('Время') 
    plt.grid()   
    plt.plot(x, y)
    plt.show()