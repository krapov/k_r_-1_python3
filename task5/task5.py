import time
import methods
import get_slay as g_s
import graph as gh


def calc_slay(method):
    '''Вычисляет СЛАУ различных размерностей
    и замеряет затраченное на это время
    '''
    if method == 1:
        method = methods.cramer
    elif method == 2:
        method = methods.gauss
    else:
        exit()
    
    nxn = []
    elaps_time = []
    #начинаем вычислять СЛАУ различной размерности
    for i in range(7, 15, 1):
        nxn.append(i)
        #создание матрицы для решения СЛАУ
        A = g_s.gen_matrix_a(i)
        B = g_s.gen_matrix_b(i)

        #запускаем секундомер
        start_time = time.time() 
        print(method(A, B))
        #останавливаем секундомер и вычисляем затраченное время
        stop_time = time.time()
        elaps_time.append(stop_time - start_time)
    #возвращаем размерности СЛАУ и время решений
    return nxn, elaps_time


if __name__ == '__main__':
    print('Выберете метод решения слау:')
    method = int(input('1. Cramer\n2. Gauss\n'))
    nxn, elaps_time = calc_slay(method)
    #создаём график
    gh.graph(nxn, elaps_time)