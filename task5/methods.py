import determinant as d
import slay_opertions as slay

def cramer(A, B):
    '''Вычисляет СЛАУ различных размерностей
    и замеряет затраченное на это время
    '''
    det = d.get_matrix_deternminant(A)
    d_n = []
    for j in range(len(A)):
        col_a_n = []

        #заменяем поочерёдно столбцы матрицы а матрицей b
        for k in range(j):
            col_a_n.append(A[k][j])
            A[k][j] = B[k]
        d_n.append(d.get_matrix_deternminant(A))

        #возвращаём матрицу а в исходное состояние
        for k in range(j):
            col_a_n.append(A[k][j])
            A[k][j] = col_a_n[k]

        #находим решения СЛАУ
        X = []
        for j in range(len(d_n)):
            X.append(d_n[j] / det)

    return X


def gauss(A, B):
    '''решение системы методом Гаусса (приведением к треугольному виду)'''
    column = 0
    while (column < len(B)):
        #ищем максимальный по модулю элемент в {colomn+1}-м столбце
        current_row = None
        for r in range(column, len(A)):
            if current_row is None or abs(A[r][column]) > abs(A[current_row][column]):
                current_row = r
        
        if current_row is None:
            #решений нет"
            return None
        
        if current_row != column:
            #переставляем строку с найденным элементом повыше
            slay.swap_rows(A, B, current_row, column)
        
        #нормализуем строку с найденным элементом
        slay.divide_row(A, B, column, A[column][column])
        
        #обрабатываем нижележащие строки
        for r in range(column + 1, len(A)):
            slay.combine_rows(A, B, r, column, -A[r][column])
        column += 1

    #матрица приведена к треугольному виду, считаем решение
    X = [0 for b in B]
    for i in range(len(B) - 1, -1, -1):
        X[i] = B[i] - sum(x * a for x, a in zip(X[(i + 1):], A[i][(i + 1):]))
    return X


