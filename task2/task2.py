import os
import time


frames = []
printing = False
while True:
    #считываем строку
    try:
        line = input()
    except EOFError:
        break
    
    #ищем границы кадра
    if line == '```':
        printing = not printing
        #если кадр начался, то добавляем для него массив 
        if printing == True:
            frames.append([])
        continue
    
    #если строка находится в границах кадра, то сохраняем её
    if printing == True:
        frames[-1].append(line)
    
def play_anim(frames):
    '''делает анимацию из ранее созданных кадров'''
    while True:
        for i in range(len(frames)):
            os.system('clear')
            for j in range(len(frames[i])):
                print(f'\033[96m{frames[i][j]}\033[0m')
            print(f'\033[31mДля выхода нажмите "ctrl + c"\033[96m')
            time.sleep(0.6)
      
#обработчик 'KeyboardInterrupt' для выхода из бесконечного показа анимации
try:
    play_anim(frames)
except KeyboardInterrupt:
    os.system('clear') 
    print('Bye-Bye!')