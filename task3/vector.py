import random


def gen_int_vectors(n):
    x_int = [random.randint(-100, 100) for i in range(n)]
    y_int = [random.randint(-100, 100) for i in range(n)]
    return x_int, y_int


def gen_float_vectors(n):
    x_float = [random.uniform(-100, 100) for i in range(n)]
    y_float = [random.uniform(-100, 100) for i in range(n)]
    return x_float, y_float


